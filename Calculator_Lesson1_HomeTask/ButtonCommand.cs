﻿using System;
using System.Windows.Input;

namespace Calculator_Lection1 {
	public class ButtonCommand: ICommand
	{
		private readonly Action<string> _action;
		public ButtonCommand(Action<string> action){
			_action = action;
		}
		public bool CanExecute(object parameter){
			return _action != null && parameter != null;
		}

		public void Execute(object parameter){			
			_action.Invoke(parameter.ToString());
		}

		public event EventHandler CanExecuteChanged;
	}
}
