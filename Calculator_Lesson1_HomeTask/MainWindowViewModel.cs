﻿using System;
using System.ComponentModel;
using System.Data;
using System.Runtime.CompilerServices;
using Calculator_Lesson1_HomeTask;
using Calculator_Lesson1_HomeTask.Annotations;

namespace Calculator_Lection1 {
	public class MainWindowViewModel : INotifyPropertyChanged {
		public MainWindowViewModel() {
			ButtonPress = new ButtonCommand(ButtonPressed);			
		}

		public ButtonCommand ButtonPress {
			get; private set;
		}

		private string _result;
		public string Result {
			get {
				return _result;
			}
			set {
				if(_result == value) {
					return;
				}
				_result = value;
				OnPropertyChanged(nameof(Result));
			}
		}

		private void ButtonPressed(string commandParametr) {			
			if(String.IsNullOrEmpty(Result) || Result.Equals(Constants.InvalidResult)) {
				Result = string.Empty;
			}

			switch(commandParametr) {
				case Constants.Calculate:
					Calculate();
					break;
				case Constants.Clear:
					ClearResult();
					break;
				case Constants.Remove:
					RemoveLastSimbol();
					break;
				default:
                    int lastSymbol;
                    string space = " ";
                    if (!String.IsNullOrEmpty(Result) 
                        && int.TryParse(Result[Result.Length - 1].ToString(), out lastSymbol)
                        && int.TryParse(commandParametr, out lastSymbol)){
                        space = string.Empty;
                    }
					Result += $"{space}{commandParametr}"; 
					break;
			}
		}

		private void ClearResult() {
			Result = string.Empty; 
		}

		private void RemoveLastSimbol() {
			if(String.IsNullOrEmpty(Result)) {
				return;
			}
			Result = Result.Remove(Result.Length - 1);
		}

		private void Calculate() {
			try {
				double result = Convert.ToDouble(new DataTable().Compute(Result.Replace(" ", string.Empty), null));
				Result = result.ToString();
			} catch(Exception) {
				Result = Constants.InvalidResult;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
