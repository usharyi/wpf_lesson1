﻿using System.Windows;
using Calculator_Lection1;

namespace Calculator_Lesson1_HomeTask {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
			DataContext = new MainWindowViewModel();
		}
	}
}
