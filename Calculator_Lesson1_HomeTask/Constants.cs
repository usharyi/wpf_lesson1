﻿namespace Calculator_Lection1 {
	public class Constants{
		public const string Calculate = "=";
		public const string Clear = "CE";
		public const string Remove = "<--";
		public const string InvalidResult = "Jackpot";
        public const string Dot = ".";
    }
}
