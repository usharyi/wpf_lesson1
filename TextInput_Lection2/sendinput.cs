using System;
using System.Runtime.InteropServices;

namespace TextInput_Lection2 {
	internal class KeyInput {
		#region Imports

		[DllImport("user32.dll", SetLastError = true)]
		public static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);
		#endregion

		public static void SendKeyAsInput(System.Windows.Forms.Keys key) {
			INPUT structure = new INPUT();
			structure.type = (int) InputType.INPUT_KEYBOARD;
			structure.ki.wVk = (short) key;
			structure.ki.dwFlags = (int) KEYEVENTF.KEYDOWN;

			INPUT input2 = new INPUT();
			input2.type = (int) InputType.INPUT_KEYBOARD;
			input2.ki.wVk = (short) key;
			input2.mi.dwFlags = (int) KEYEVENTF.KEYUP;


			INPUT[] pInputs = new INPUT[] { structure, input2 };

			SendInput(2, pInputs, Marshal.SizeOf(structure));
		}


		[StructLayout(LayoutKind.Explicit)]
		public struct INPUT {
			[FieldOffset(4)]
			public HARDWAREINPUT hi;
			[FieldOffset(4)]
			public KEYBDINPUT ki;
			[FieldOffset(4)]
			public MOUSEINPUT mi;
			[FieldOffset(0)]
			public int type;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct MOUSEINPUT {
			public int dx;
			public int dy;
			public int mouseData;
			public int dwFlags;
			public int time;
			public IntPtr dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct KEYBDINPUT {
			public short wVk;
			public short wScan;
			public int dwFlags;
			public int time;
			public IntPtr dwExtraInfo;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct HARDWAREINPUT {
			public int uMsg;
			public short wParamL;
			public short wParamH;
		}

		[Flags]
		public enum InputType {
			INPUT_MOUSE = 0,
			INPUT_KEYBOARD = 1,
			INPUT_HARDWARE = 2
		}

		[Flags]
		public enum KEYEVENTF {
			KEYDOWN = 0,
			EXTENDEDKEY = 0x0001,
			KEYUP = 0x0002,
			UNICODE = 0x0004,
			SCANCODE = 0x0008,
		}
	}
}