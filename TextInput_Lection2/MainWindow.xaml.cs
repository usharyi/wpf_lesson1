﻿using System.Globalization;
using System.Windows;
using WinFormKeys = System.Windows.Forms;
using System.Windows.Input;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;

namespace TextInput_Lection2 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}
		private readonly string _currentCultureDecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

		private void OnPreviewTextInput(object sender, TextCompositionEventArgs e) {
			if(IsContainsSeparator(e.Text)) {
				if(IsContainsSeparator(TextBox.Text) || string.IsNullOrEmpty(TextBox.Text)) {
					e.Handled = true;
					return;
				}
				if(e.Text != _currentCultureDecimalSeparator) {
					if(e.Text == ".") {
						MakeMagic(WinFormKeys.Keys.Oemcomma);
					}
					if(e.Text == ",") {
						MakeMagic(WinFormKeys.Keys.OemPeriod);
					}
					e.Handled = true;
					return;
				}				
			}

			double i;
			if(!double.TryParse(e.Text, out i)) {
				e.Handled = true;
			}
		}

		private void OnPreviewKeyDown(object sender, KeyEventArgs e) {
			if(e.Key == Key.Space) {
				e.Handled = true;
			}		
		}

		private void MakeMagic(WinFormKeys.Keys key) {
			KeyInput.SendKeyAsInput(key);
		}

		private void Window_GotFocus(object sender, RoutedEventArgs e) {
			TextBox.Focus();
		}

		private bool IsContainsSeparator(string text) {
			return text.Contains(".") || text.Contains(",");
		}
	}
}
