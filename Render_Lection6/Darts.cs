﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Render_Lection6.Annotations;

namespace Render_Lection6 {
	public class Darts : FrameworkElement, INotifyPropertyChanged {
		static Darts() {
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(Darts),
				new FrameworkPropertyMetadata(typeof(Darts)));
		}

		private readonly int[] _scale = { 6, 13, 4, 18, 1, 20, 5, 12, 9, 14, 11, 8, 16, 7, 19, 3, 17, 2, 15, 10, 6 };
		private readonly DrunkMouse _drunkMouse = new DrunkMouse();

		/// <summary>
		/// Radiuses for inner circles
		/// </summary>
		private double[] _circleRadiuses;

		#region Properties
		public static readonly DependencyProperty CenterPointProperty = DependencyProperty.Register(
			"CenterPoint", typeof(Point), typeof(Darts),
			new FrameworkPropertyMetadata(new Point(0, 0)));

		public static readonly DependencyProperty RadiusProperty = DependencyProperty.Register(
			"Radius", typeof(double), typeof(Darts),
			new FrameworkPropertyMetadata(0d));

		public static readonly DependencyProperty DrunkMouseProperty = DependencyProperty.Register(
			"DrunkMouse", typeof(bool), typeof(Darts),
			new FrameworkPropertyMetadata(false));

		public bool DrunkMouse {
			get {
				return (bool) GetValue(DrunkMouseProperty);
			}
			set {
				SetValue(DrunkMouseProperty, value);
			}
		}

		public double Radius {
			get {
				return (double) GetValue(RadiusProperty);
			}
			set {
				SetValue(RadiusProperty, value);
			}
		}

		public Point CenterPoint {
			get {
				return (Point) GetValue(CenterPointProperty);
			}
			set {
				SetValue(CenterPointProperty, value);
			}
		}

		private int _lastScore;
		public int LastScore {
			get {
				return _lastScore;
			}
			private set {
				_lastScore = value;
				OnPropertyChanged(nameof(LastScore));
			}
		}

		#endregion


		public static RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Darts));

		public event RoutedEventHandler Click {
			add {
				AddHandler(ClickEvent, value);
			}
			remove {
				RemoveHandler(ClickEvent, value);
			}
		}

		protected virtual void OnClick() {
			RoutedEventArgs args = new RoutedEventArgs(ClickEvent, this);

			RaiseEvent(args);
		}

		protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e) {
			base.OnMouseLeftButtonUp(e);
			SetScore(e.GetPosition(this));

			OnClick();
		}	

		protected override void OnMouseMove(MouseEventArgs e) {
			base.OnMouseMove(e);
			if(DrunkMouse) {
				var random = new Random();
				var x = random.Next(-1, 2);
				var y = random.Next(-1, 2);
				if(x != 0 && y != 0) {
					var p = _drunkMouse.GetCursorPosition();
					_drunkMouse.SetPosition((int) p.X + x, (int) p.Y + y);
				}
			}
		}

		public void SetScore(Point userHit) {
			var hitVector = userHit - CenterPoint;
			hitVector.Y = -hitVector.Y;
			var angle = Vector.AngleBetween(new Vector(GetX(351, Radius), GetY(351, Radius)), hitVector);
			if(angle < 0)
				angle = 360 + angle;
			int scaleIndex = Math.Abs((int) angle / 18);
			var distanceFromCenter = hitVector.Length;
			if(distanceFromCenter > _circleRadiuses[5]) {
				LastScore = 0;
				return;
			}
			if(distanceFromCenter > _circleRadiuses[4]) {
				LastScore = _scale[scaleIndex] * 2;
				return;
			}
			if(distanceFromCenter > _circleRadiuses[3]) {
				LastScore = _scale[scaleIndex];
				return;
			}
			if(distanceFromCenter > _circleRadiuses[2]) {
				LastScore = _scale[scaleIndex] * 3;
				return;
			}
			if(distanceFromCenter > _circleRadiuses[1]) {
				LastScore = _scale[scaleIndex];
				return;
			}
			if(distanceFromCenter > _circleRadiuses[0]) {
				LastScore = 25;
				return;
			}
			LastScore = 50;
		}

		protected override void OnRender(DrawingContext drawingContext) {
			_circleRadiuses = new double[7] { Radius * 0.05, Radius * 0.10, Radius * 0.40, Radius * 0.45, Radius * 0.75, Radius * 0.80, Radius * 0.90 };

			// Calculate current DPI.
			Matrix m = PresentationSource.FromVisual(this).CompositionTarget.TransformToDevice;
			double dpiFactor = 1 / m.M11;

			// Let the pen width be one PHYSICAL pixel
			var pen = new Pen(Brushes.Black, 1 * dpiFactor);

			// Draw Lines
			drawingContext.DrawEllipse(Brushes.Black, pen, CenterPoint, Radius, Radius);

			drawingContext.DrawEllipse(Brushes.Green, pen, CenterPoint, _circleRadiuses[5], _circleRadiuses[5]);
			DrawAngleSectorsFromCenter(drawingContext, pen, Brushes.Red, _circleRadiuses[5]);

			drawingContext.DrawEllipse(Brushes.Cornsilk, pen, CenterPoint, _circleRadiuses[4], _circleRadiuses[4]);
			DrawAngleSectorsFromCenter(drawingContext, pen, Brushes.Black, _circleRadiuses[4]);

			drawingContext.DrawEllipse(Brushes.Green, pen, CenterPoint, _circleRadiuses[3], _circleRadiuses[3]);
			DrawAngleSectorsFromCenter(drawingContext, pen, Brushes.Red, _circleRadiuses[3]);

			drawingContext.DrawEllipse(Brushes.Cornsilk, pen, CenterPoint, _circleRadiuses[2], _circleRadiuses[2]);
			DrawAngleSectorsFromCenter(drawingContext, pen, Brushes.Black, _circleRadiuses[2]);

			drawingContext.DrawEllipse(Brushes.Green, pen, CenterPoint, _circleRadiuses[1], _circleRadiuses[1]);
			drawingContext.DrawEllipse(Brushes.Red, pen, CenterPoint, _circleRadiuses[0], _circleRadiuses[0]);

			DrawNumbers(drawingContext);
		}

		private void DrawNumbers(DrawingContext drawingContext) {
			int i = 0;
			for(int degree = 0 ; degree < 360 ; degree += 18) {
				double y = GetY(degree, _circleRadiuses[6]) + Radius * 0.07;
				double x = GetX(degree, _circleRadiuses[6]) - Radius * 0.07;

				drawingContext.DrawText(new FormattedText(_scale[i].ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Verdana"), Radius / 8, Brushes.White), new Point(CenterPoint.X + x, CenterPoint.Y - y));
				i++;
			}
		}

		private void DrawAngleSectorsFromCenter(DrawingContext drawingContext, Pen pen, Brush brush, double radius) {
			for(int degree = 9 ; degree <= 351 ; degree += 36) {
				var lineSegment = new LineSegment(new Point(CenterPoint.X + GetX(degree, radius), CenterPoint.Y - GetY(degree, radius)), true);
				var arcSegment = new ArcSegment {
					Point = new Point(CenterPoint.X + GetX(degree + 18, radius), CenterPoint.Y - GetY(degree + 18, radius)),
					Size = new Size(radius, radius),
					SweepDirection = SweepDirection.Counterclockwise
				};
				var figure = new PathFigure(CenterPoint, new PathSegment[] { lineSegment, arcSegment }, true);
				var geometry = new PathGeometry(new[] { figure });
				drawingContext.DrawGeometry(brush, pen, geometry);
			}
		}

		private double GetY(int degree, double radius) {
			return Math.Sin(GetAngle(degree)) * radius;
		}

		private double GetX(int degree, double radius) {
			return Math.Cos(GetAngle(degree)) * radius;
		}

		private double GetAngle(int degree) {
			return Math.PI * degree / 180.0;
		}

		public event PropertyChangedEventHandler PropertyChanged;
		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
