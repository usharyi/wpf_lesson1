﻿using System.Windows;

namespace Render_Lection6 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

		private void Darts_OnClick(object sender, RoutedEventArgs e){
			ScoreTb.Text = ((Darts) sender).LastScore.ToString();
		}
	}
}
