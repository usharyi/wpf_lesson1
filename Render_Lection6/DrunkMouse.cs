﻿using System.Runtime.InteropServices;
using System.Windows;

namespace Render_Lection6 {
	class DrunkMouse {
		public void SetPosition(int a, int b) {
			SetCursorPos(a, b);
		}

		[DllImport("User32.dll")]
		private static extern bool SetCursorPos(int X, int Y);

		/// <summary>
		/// Struct representing a point.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct POINT {
			public int X;
			public int Y;

			public static implicit operator Point(POINT point) {
				return new Point(point.X, point.Y);
			}
		}

		/// <summary>
		/// Retrieves the cursor's position, in screen coordinates.
		/// </summary>
		/// <see>See MSDN documentation for further information.</see>
		[DllImport("user32.dll")]
		public static extern bool GetCursorPos(out POINT lpPoint);

		public Point GetCursorPosition() {
			POINT lpPoint;
			GetCursorPos(out lpPoint);
		
			return lpPoint;
		}
	}
}
